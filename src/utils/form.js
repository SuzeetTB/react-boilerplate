//utils folder

/**
 * Returns error string in formik
 * @param {Object} errors Formik error object
 * @param {Object} touched Formik touched object
 * @param {String} name Name of the field
 * @returns {String | false} Error string or false
 */
function getErrorMessage(errors, touched, name) {
  return errors[name] && touched[name] ? errors[name] : false;
}

export { getErrorMessage };
