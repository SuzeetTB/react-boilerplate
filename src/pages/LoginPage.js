import { makeStyles } from "@material-ui/core";
import React from "react";
import Login from "../components/Login";

const useStyles = makeStyles((theme) => ({
  container: {
    maxWidth: "500px",
    padding: "30px",
    margin: "100px auto",
  },
}));

const LoginPage = () => {
  const styles = useStyles();

  const demoOnsubmit = (values) => {
    console.table(values);
  };

  return (
    <div className={styles.container}>
      <Login onSubmit={demoOnsubmit} />
    </div>
  );
};

export default LoginPage;
