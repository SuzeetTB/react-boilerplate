import { ThemeProvider } from "@material-ui/styles";
import React from "react";
import { Route, Switch } from "react-router-dom";
import LoginPage from "../pages/LoginPage";
import theme from "../theme/defaultTheme";

/**
 * This will serve as our primary entry point and configs.
 */
const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <Switch>
          <Route path="/login">
            <LoginPage />
          </Route>
          <Route path="/">
            <div>This is a homepage</div>
          </Route>
        </Switch>
      </div>
    </ThemeProvider>
  );
};

export default App;
