import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useFormik } from "formik";
import * as Yup from "yup";
import { getErrorMessage } from "../utils/form";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const initialValues = {
  email: "",
  password: "",
  remember: false,
};

const _onSubmit = (values) => {
  console.log(values);
};

const validationSchema = Yup.object().shape({
  email: Yup.string().email("Must be a valid email").required("Required"),
  password: Yup.string()
    .min(6, "Should be minimum of 6 characters")
    .max(16, "Must be less than 16 characters")
    .required("Required"),
  remember: Yup.bool(),
});

function Login({ onSubmit }) {
  const classes = useStyles();

  const formik = useFormik({ initialValues, onSubmit, validationSchema });

  return (
    <div>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} onSubmit={formik.handleSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            id="email"
            label="Email Address"
            autoComplete="email"
            autoFocus
            {...formik.getFieldProps("email")}
            error={Boolean(
              getErrorMessage(formik.errors, formik.touched, "email")
            )}
            helperText={getErrorMessage(formik.errors, formik.touched, "email")}
          />
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            {...formik.getFieldProps("password")}
            error={Boolean(
              getErrorMessage(formik.errors, formik.touched, "password")
            )}
            helperText={getErrorMessage(
              formik.errors,
              formik.touched,
              "password"
            )}
          />
          <FormControlLabel
            control={
              <Checkbox
                value="remember"
                name="remember"
                checked={formik.values.remember}
                onChange={(e) => {
                  formik.setFieldValue("remember", e.target.checked);
                  formik.setFieldTouched("remember", true);
                }}
                color="primary"
              />
            }
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link to="/forgot-password" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link to="/signup" variant="body2">
                Don't have an account? Sign Up
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </div>
  );
}

Login.defaultProps = {
  onSubmit: _onSubmit,
};

Login.propTypes = {
  onSubmit: PropTypes.func,
};

export default Login;
